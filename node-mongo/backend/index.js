const express = require("express");
const bodyParser = require('body-parser');
const router = require("./router");
const cors = require('cors');
const port = 3000;


const app = express();
app.use("/", router);

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});