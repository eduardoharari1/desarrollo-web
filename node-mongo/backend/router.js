const express = require("express");
var MongoClient = require("mongodb").MongoClient,
  ObjectID = require("mongodb").ObjectID;
const bodyParser = require("body-parser");
const router = express.Router();
const cors = require("cors");

const mongoUrl =
  "mongodb://localhost:27017/?readPreference=primary&appname=MongoDB%20Compass&ssl=false";

router.use(cors());
router.use(bodyParser.json());
router.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

router.get("/", function (req, res) {
  res.send({
    data: "hello",
  });
});

router.get("/articulos", function (req, res) {
  MongoClient.connect(mongoUrl, {
      useUnifiedTopology: true,
    })
    .then((client) => {
      const db = client.db("inventario");
      const itemsCollection = db.collection("articulos");

      itemsCollection
        .aggregate(
          [{
            $lookup: {
              from: 'proveedores',
              localField: 'id_proveedor',
              foreignField: '_id',
              as: 'proveedor'
            }
          }]
        ).toArray()
        .then((results) => {
          res.send(results);
        });

      //   .find()
      //   .toArray()
      //   .then((results) => {
      //     res.send(results);
      //   });
    })
    .catch((error) => console.error(error));
});


router.post("/articulo", function (req, res) {
  let matchId = req.body.id;

  MongoClient.connect(mongoUrl, {
      useUnifiedTopology: true,
    })
    .then((client) => {
      const db = client.db("inventario");
      const itemsCollection = db.collection("articulos");

      itemsCollection
        .aggregate(
          [{
            $match: {
              _id: ObjectID(matchId),
            }
          }, {
            $lookup: {
              from: 'proveedores',
              localField: 'id_proveedor',
              foreignField: '_id',
              as: 'proveedor'
            }
          }]
        )
      .toArray()
      .then((results) => {
        res.send(results);
      });
    })
    .catch((error) => console.error(error));
});


router.post("/articulos/agregar", function (req, res) {

  MongoClient.connect(mongoUrl, {
      useUnifiedTopology: true,
    })
    .then((client) => {
      const db = client.db("inventario");
      const itemsCollection = db.collection("articulos");

      // let query = {
      //   _id: ObjectID(req.body.id_proveedor)
      // };

      // async function getNombreProveedor() {
      //   const supliersCollection = db.collection("proveedores");
      //   const proveedor = await supliersCollection.findOne(query, {});
      //   // since this method returns the matched document, not a cursor, print it directly
      //   console.log("Returning this:", proveedor);
      //   return proveedor.nombre;
      // }

      // getNombreProveedor()
      // .then(() => {

      let newItem = {
        nombre: req.body.nombre,
        precio_proveedor: parseInt(req.body.precio_proveedor),
        precio_cliente: parseInt(req.body.precio_cliente),
        id_proveedor: ObjectID(req.body.id_proveedor),
        // proveedor: nombre_proveedor,
        cantidad: parseInt(req.body.cantidad),
      };

      itemsCollection.insertOne(newItem);
      res.send();
      // })
    })
    .catch((error) => console.error(error));
});


router.put("/articulos/editar", function (req, res) {

  let matchId = req.body.id;
  let updatedItem = {
    nombre: req.body.nombre,
    precio_proveedor: parseInt(req.body.precio_proveedor),
    precio_cliente: parseInt(req.body.precio_cliente),
    id_proveedor: ObjectID(req.body.id_proveedor),
    cantidad: parseInt(req.body.cantidad),
  };

  MongoClient.connect(mongoUrl, {
      useUnifiedTopology: true,
    })
    .then((client) => {
      const db = client.db("inventario");
      const itemsCollection = db.collection("articulos");


      itemsCollection.updateOne({
        _id: ObjectID(matchId)
      }, {
        $set : updatedItem
      });
      res.send();
    })
    .catch((error) => console.error(error));
});


router.put("/articulos/agregar/cantidad", function (req, res) {
  let matchId = req.body.id;
  let cantidadAgregar = parseInt(req.body.cantidad);

  MongoClient.connect(mongoUrl, {
      useUnifiedTopology: true,
    })
    .then((client) => {
      const db = client.db("inventario");
      const itemsCollection = db.collection("articulos");

      itemsCollection.updateOne({
        _id: ObjectID(matchId)
      }, {
        $inc: {
          cantidad: cantidadAgregar
        }
      });

      res.send();
    })
    .catch((error) => console.error(error));
});

router.put("/articulos/eliminar/cantidad", function (req, res) {
  let matchId = req.body.id;
  let cantidadEliminar = -parseInt(req.body.cantidad);

  MongoClient.connect(mongoUrl, {
      useUnifiedTopology: true,
    })
    .then((client) => {
      const db = client.db("inventario");
      const itemsCollection = db.collection("articulos");

      itemsCollection.updateOne({
        _id: ObjectID(matchId)
      }, {
        $inc: {
          cantidad: cantidadEliminar
        }
      });

      res.send();
    })
    .catch((error) => console.error(error));
});

router.delete("/articulos/eliminar", function (req, res) {
  let matchId = req.body.id;
  MongoClient.connect(mongoUrl, {
      useUnifiedTopology: true,
    })
    .then((client) => {
      const db = client.db("inventario");
      const itemsCollection = db.collection("articulos");

      itemsCollection.deleteOne({
        _id: ObjectID(matchId)
      });
      res.send();
    })
    .catch((error) => console.error(error));
});



// Proveedores
router.get("/proveedores", function (req, res) {
  MongoClient.connect(mongoUrl, {
      useUnifiedTopology: true,
    })
    .then((client) => {
      const db = client.db("inventario");
      const itemsCollection = db.collection("proveedores");

      itemsCollection
        .find()
        .toArray()
        .then((results) => {
          res.send(results);
        });
    })
    .catch((error) => console.error(error));
});


router.post("/proveedor", function (req, res) {
  let matchId = req.body.id;

  MongoClient.connect(mongoUrl, {
      useUnifiedTopology: true,
    })
    .then((client) => {
      const db = client.db("inventario");
      const itemsCollection = db.collection("proveedores");

      itemsCollection
        .find({
          _id: ObjectID(matchId)
        })
        .toArray()
        .then((results) => {
          res.send(results);
        });
    })
    .catch((error) => console.error(error));
});


router.post("/proveedores/agregar", function (req, res) {

  let newProveedor = {
    nombre: req.body.nombre
  };

  MongoClient.connect(mongoUrl, {
      useUnifiedTopology: true,
    })
    .then((client) => {
      const db = client.db("inventario");
      const itemsCollection = db.collection("proveedores");

      itemsCollection.insertOne(newProveedor);
      res.send();
    })
    .catch((error) => console.error(error));
});


router.put("/proveedores/editar", function (req, res) {

  let matchId = req.body.id;
  let nombre = req.body.nombre;

  MongoClient.connect(mongoUrl, {
      useUnifiedTopology: true,
    })
    .then((client) => {
      const db = client.db("inventario");
      const itemsCollection = db.collection("proveedores");

      itemsCollection.updateOne({
        _id: ObjectID(matchId)
      }, {
        $set: {
          nombre: nombre
        }
      });
    })
    .catch((error) => console.error(error));
  res.send();
});


router.delete("/proveedores/eliminar", function (req, res) {
  let matchId = req.body.id;

  MongoClient.connect(mongoUrl, {
      useUnifiedTopology: true,
    })
    .then((client) => {
      const db = client.db("inventario");
      const itemsCollection = db.collection("proveedores");

      itemsCollection.deleteOne({
        _id: ObjectID(matchId)
      });
      res.send();
    })
    .catch((error) => console.error(error));
});

module.exports = router;